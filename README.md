Yelp API - Recherche de restaurants à Paris
Ce projet est un exemple d'utilisation de l'API Yelp pour rechercher des restaurants à Paris. Le code est écrit en Python et utilise la bibliothèque requests.

Prérequis
Pour utiliser ce code, vous aurez besoin des éléments suivants :

Une clé d'API valide pour l'API Yelp
La bibliothèque requests installée dans votre environnement Python
Utilisation
Ajoutez votre clé d'API Yelp à la variable api_key dans le code.
Exécutez le code en utilisant Python.
Les informations sur les restaurants à Paris sont affichées dans la console.
Licence
Ce projet est sous licence MIT. Consultez le fichier LICENSE pour plus d'informations.

Exécutez les tests en utilisant Python.
Les résultats des tests sont affichés dans la console.
Tests
test_search_restaurants: Recherche des restaurants à Paris. Vérifie que la requête renvoie un code de statut 200 et que la liste des restaurants est non vide.
test_search_with_incorrect_filters: Recherche des restaurants à Paris avec des filtres incorrects. Vérifie que la requête renvoie un code de statut 400 et que l'erreur contient le message "Incorrect parameter". Si la requête renvoie un code de statut 200, vérifie que la liste des restaurants est non vide.
test_add_review: Ajout d'une critique pour un restaurant à Paris. Recherche d'abord un restaurant pour obtenir son ID, puis ajoute une critique avec une note de 5 et un texte "Great restaurant!". Vérifie que la requête renvoie un code de statut 200.

Ce projet a été réalisé par Thierno Yaya AW.
