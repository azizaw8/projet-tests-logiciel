import unittest
import requests

api_key = "CShcAhDRq8iz7SWiAmQWxvwXTDEI3xaSteNMZbFGHxKlnwwhLkG1hiemAfNL6SVwDHky0tH06ElzqPjZVa7dSDe_bTCuNegGa2oLOr-V8OwptIZ6azxbpp0qvTQgZHYx"
url = "https://api.yelp.com/v3/businesses/search"
headers = {
    "Authorization": "Bearer " + api_key
}

class TestYelpAPI(unittest.TestCase):

    def test_search_restaurants(self):
        params = {
            "term": "restaurants",
            "location": "Paris"
        }
        response = requests.get(url, headers=headers, params=params)
        self.assertEqual(response.status_code, 200)
        result = response.json()
        self.assertGreater(len(result["businesses"]), 0)

    def test_search_with_incorrect_filters(self):
        params = {
            "term": "restaurants",
            "location": "Paris",
            "incorrect_filter": "test"
        }
        response = requests.get(url, headers=headers, params=params)
        if response.status_code == 400:
            result = response.json()
            self.assertIn("Incorrect parameter", result.get("error", {}).get("description", ""))
        else:
            self.assertEqual(response.status_code, 200)
            result = response.json()
            self.assertGreater(len(result["businesses"]), 0)

    def test_add_review(self):
        # First, search for a restaurant to get its ID
        params = {
            "term": "restaurants",
            "location": "Paris"
        }
        response = requests.get(url, headers=headers, params=params)
        result = response.json()
        self.assertGreater(len(result["businesses"]), 0)
        restaurant_id = result["businesses"][0]["id"]

        # Then, add a review for the restaurant
       # review_url = f"https://api.yelp.com/v3/businesses/{restaurant_id}/reviews"
        #review_data = {
         #   "rating": 5,
          #  "text": "Great restaurant!"
        #}
        #review_response = requests.post(review_url, headers=headers, json=review_data)
        #print(review_response.content)
        #self.assertEqual(review_response.status_code, 200)
        

if __name__ == '__main__':
    unittest.main()
